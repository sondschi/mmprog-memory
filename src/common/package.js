/**
 * @author    Wolfgang Kowarschick
 * @copyright 2012, Wolfgang Kowarschick
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted under the terms of the
 * Creative Commons License Attribution-NonCommercial-ShareAlike 3.0 Unported
 * (CC BY-NC-SA 3.0: http://creativecommons.org/licenses/by-nc-sa/3.0/).
 */

/**
 *  Creates a chain of nested objects.
 *  <p>
 *  For instance: The following function call
 *  <pre>
 *  $pkg$("ldvcs.view");
 *  </pre>
 *  results in the creation of three objects (if they do not already exist):
 *  <pre>
 *  $pkg$
 *  $pkg$.ldvcs
 *  $pkg$.ldvcs.view
 *  </pre>
 *  </p>
 *
 *  @param {String} p_name   A package name, i.e. one or more name literals
 *                           separated by dots.
 *  @return The last package object of the package object chain, if
 *          it existed already. Otherwise <code>null</code> is returned.
 *          That is to say, $pkg$ can be utilized to access an arbitrary
 *          package object via its string name.
 */
function $pkg$(p_name) {
    var l_subpackages = p_name.split(".");      // all literal names
    var l_number = l_subpackages.length;   // number of literal names
    var l_package_object = $pkg$;                  // the root object
    var l_return_result = true;
    for (var i = 0; i < l_number; i++)             // for each literal name
    {
        var l_subpackage_name = l_subpackages[i];  // the current literal name
        // Fetch the associated package object.
        var l_subpackage_object = l_package_object[l_subpackage_name];
        // If it does not exist: create it.
        if (!l_subpackage_object) {
            l_return_result = false;
            l_package_object[l_subpackage_name] = l_subpackage_object = {};
        }
        ;
        // Set the current subpackage object to be the new package object,
        // for which the next subpackage object hast to be created.
        l_package_object = l_subpackage_object;
    }
    ;
    return l_return_result ? l_package_object : null;
};