function earlyMixin(sink, source) {
    for (var property in source) {
        sink[property] = source[property];
    }
}

function getTransformation(el, posX, posY, scale) {
    var oldX, oldY;

    oldX = el.getBBox(false).x + el.getBBox(false).width / 2;
    oldY = el.getBBox(false).y + el.getBBox(false).height / 2;

    el.x = posX;
    el.y = posY;
    el.scale = scale;
    el.dx = 0 - oldX;
    el.dy = 0 - oldY;

    return "t" + (el.x + el.dx) + "," + (el.y + el.dy) + "s" + el.scale;
};