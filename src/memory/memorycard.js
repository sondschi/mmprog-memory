$pkg$("memory");

(function () {
    "use strict";


    function MemoryCard(id, x, y, width, height, callback) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.isOpen = false;

        this.callback = callback;

    };


    var prototype = {
        draw: draw,
        remove: remove,
        open: open,
        close: close
    }

    function open() {

        if (!this.isOpen) {

            var self = this;

            var ready = 0;
            var l = this.set.length;

            for (var i = 0; i < l; i++) {

                var el = this.set[i];

                el.animate({opacity: 0}, 1000, null, function () {
                    ready++;

                    if (ready == 2)
                        self.callback(self.id, self);
                });

            }
            this.isOpen = true;
        }

    }

    function close() {

        this.isOpen = false;

        var l = this.set.length;

        for (var i = 0; i < l; i++) {

            var el = this.set[i];
            el.attr({opacity: 1});
        }

    }


    function remove() {
        this.card.remove();
    }


    function draw(paper, color, motiv) {

        // background
        var shaddow = paper.filter(Snap.filter.shadow(0, 2, 3));
        var bg = paper.rect(this.x, this.y, this.width, this.height);
        bg.attr({
            'fill': "#888888",
            filter: shaddow
        });


        // Motiv
        motiv.select('.clothes').attr('fill', color);

        var transformation = getTransformation(motiv, this.x + this.width / 2, this.y + this.height / 2, 0.4);
        motiv.transform(transformation);
        paper.append(motiv);

        this.content = motiv;

        this.content.attr({
            'fill': color
        });


        // Deckblatt
        var patternSquare = paper.path("M10-5-10,15M15,0,0,15M0-5-20,15M-5,0,10,15M0-5,15,10").attr({
            fill: "#333333",
            stroke: "#cccccc",
            strokeWidth: 5,
            opacity: 1
        }).pattern(0, 0, 10, 10);

        this.colorLayer = paper.rect(this.x, this.y, this.width, this.height);
        this.colorLayer.attr({
            fill: '#fff',
            opacity: 1
        });

        this.patternLayer = paper.rect(this.x, this.y, this.width, this.height);
        this.patternLayer.attr({
            fill: patternSquare,
            opacity: 1
        });


        this.set = [this.patternLayer, this.colorLayer];


        this.card = paper.g(bg, this.content, this.colorLayer, this.patternLayer); //



        // Klick-Element
        var self = this;
        this.card.click(function () {
            console.log('click');
            self.open();
        });

    }



    earlyMixin(MemoryCard.prototype, prototype);


    $pkg$.memory.MemoryCard = MemoryCard;

})();