$pkg$("memory");

(function () {
    "use strict";


    var starBigPath = "M293.802,217.269l-75.961,10.628l-34.308,68.599l-33.581-68.958l-75.843-11.43l55.206-53.247L116.75,87.197l67.7,36.05l68.078-35.333l-13.366,75.527L293.802,217.269z";
    var starSmallPath = "M407.4,162.275l-43.846,6.135l-19.803,39.597l-19.383-39.804l-43.778-6.598l31.866-30.735l-7.254-43.674l39.078,20.809l39.295-20.395l-7.715,43.595L407.4,162.275z";


    function WinAnimation(paper) {

        var self = this;
        this.paper = paper;
        this.readyCounter = 0;

        var starBig = this.paper.path(starBigPath);
        var starSmall = this.paper.path(starSmallPath);

        starBig.attr({'fill': '#F4D40C', transform: "s0"});
        starSmall.attr({'fill': '#F4D40C'});


        starBig.animate({ transform: "S1" }, 2000, function () {

            self._ready()
        });

        var angle = 360;
        var bb = starSmall.getBBox()
        starSmall.animate({ transform: "r" + angle + "," + (bb.x + bb.width / 2) + "," + (bb.y + bb.height / 2) }, 2000, function () {
           
            self._ready()
        });

    };

    function _ready() {
        this.readyCounter++;
        if (this.readyCounter == 2){
            var text = this.paper.text(250, 270, ["Du hast ","gewonnen!"]).attr({
                "font-family": "Arial, Helvetica",
                "font-size": "40px"
            });

            text.selectAll("tspan:nth-child(2)").attr({
                fill: "#900",
                "font-weight": "bold"
            });
        }
    }


    var prototype = {
        _ready: _ready
    }


    earlyMixin(WinAnimation.prototype, prototype);


    $pkg$.memory.WinAnimation = WinAnimation;

})();