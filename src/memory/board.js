$pkg$("memory");

(function () {
    "use strict";


    // Imports
    var MemoryCard = $pkg$.memory.MemoryCard;
    var WinAnimation = $pkg$.memory.WinAnimation;


    var cardWidth = 150,
        distance = cardWidth / 3;


    function Board(svgEl, url, callback) {
        this.paper = Snap(svgEl);

        this.x = 4;
        this.y = 2;
        this.boardWidth = this.x * cardWidth + (this.x - 1) * distance;
        this.boardHeight = this.y * cardWidth + (this.y - 1) * distance;


        this.paper.node.parentNode.style.width = this.boardWidth + 5 + "px";
        this.paper.node.parentNode.style.height = this.boardHeight + 5 + "px";

        this.cardCounter = 0;
        this.pairCounter = 0;

        this.openCardId = null;
        this.openCard = null;

        this.cardLength = this.x * this.y;//this.row * this.row;
        this.places = [];

        // Platzierungen vorbereiten
        for (var i = 0; i < this.x; i++) {
            for (var j = 0; j < this.y; j++) {
                this.places.push([i, j]);
            }
        }


        this.snowman;

        var self = this;


        Snap.load(url, function (f) {
            var hands = f.select('.hands');
            var body = f.select('.body');
            var nose = f.select('.nose');
            var eyes = f.select('.eyes');
            self.clothes = f.select('.clothes');

            self.snowman = self.paper.g(hands, body, nose, eyes, self.clothes);

            callback();
        });


    };


    var prototype = {
        addMemoryCard: addMemoryCard,
        _getCardPosition: _getCardPosition,
        _getCardPlace: _getCardPlace,
        _createCard: _createCard
    }


    function _getCardPlace() {
        var random = Math.random();
        var nr = random * this.cardLength;
        var place = Math.floor(nr);

        this.cardLength--;

        var pos = this.places[place];
        this.places.splice(place, 1);


        var pos = this._getCardPosition(pos);

        this.cardCounter++;
        return pos;

    }


    function _getCardPosition(posArray) {

        var x = posArray[0];
        var y = posArray[1];


        return {
            x: (x * cardWidth + (x) * distance),
            y: (y * cardWidth + (y) * distance)
        }
    }


    function cardOpenHandler(cardId, card) {

        if (this.openCardId == null) {
            this.openCardId = cardId
            this.openCard = card;
        } else {
            if (this.openCardId == cardId) {
                this.openCard.remove();
                card.remove();

                this.cardCounter -= 2;

                if (this.cardCounter == 0) {
                    new WinAnimation(this.paper);
                }

            } else {
                card.close();
                this.openCard.close();

            }

            this.openCardId = null;
            this.openCard = null;
        }
    }


    function addMemoryCard(id, color) {

        if (this.cardCounter + 2 <= this.x * this.y) {

            this._createCard(color);
            this._createCard(color);

            this.pairCounter++;

        } else {
            console.log('Es wurden bereits alle Karten gesetzt. ');
        }

    }

    function _createCard(color) {
        var pos = this._getCardPlace();
        var card1 = new MemoryCard(this.pairCounter, pos.x, pos.y, cardWidth, cardWidth, cardOpenHandler.bind(this));
        card1.draw(this.paper, color, this.snowman.clone());
    }


    earlyMixin(Board.prototype, prototype);


    $pkg$.memory.Board = Board;

})();